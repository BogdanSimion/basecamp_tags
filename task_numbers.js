// ==UserScript==
// @name         Basecamp Add TODO Numbers
// @namespace    http://tampermonkey.net/
// @version      1.00004
// @description  Basecamp Add TODO Numbers
// @author       Bogdan Simion
// @match        https://3.basecamp.com/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=basecamp.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    setInterval(function()
 {
   if (document.getElementsByClassName('span_numbers').length != 0) return; //check if page is dinamic reloaded and already has the numbers

   //render numbers
   var drag_handle = document.getElementsByClassName('todo__drag-handle');
   for(let i = 0; i < drag_handle.length; i++) drag_handle[i].innerHTML+='<span class="span_numbers" style="font-size:11px; filter: invert(100%);">'+i+'</span>';

 }, 100);
})();
