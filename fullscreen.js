// ==UserScript==
// @name         Basecamp History on refresh + Large Screen support
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  Shows todo history inside todo page (Vanilla JavaScript) + Large Screen support
// @author       Bogdan Simion
// @match        https://3.basecamp.com/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=basecamp.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    setTimeout(function() {
        addGlobalStyle('.panel { max-width: 100% !important; }');
        addGlobalStyle('.todo__titlev2{ position: fixed;z-index: 1000000;bottom: 0px;text-align: center;width: 100%;background-color: floralwhite;font-size: 11px;font-weight: 500; }');
        addGlobalStyle('.todo__titlev2_span{filter: invert(100%);}');

        var div = document.createElement("div");
        div.innerHTML ='<div class="todo__titlev2"><span class="todo__titlev2_span">'+document.getElementsByClassName("todo__title")[0].querySelector('a').innerHTML+'    ['+document.getElementsByClassName("todo__assignee")[0].querySelector('a').innerHTML+']</span></div>';
        document.body.appendChild(div);

        let ids = window.location.href.split('/');
        if (ids[4] != 'buckets') return false;
        if (ids[6] != 'todos') return false;
        var request = new XMLHttpRequest();
        request.open("GET", "https://3.basecamp.com/"+ids[3]+"/buckets/"+ids[5]+"/recordings/"+ids[7]+"/events");
        request.onreadystatechange = function()
        {
            // Check if the request is compete and was successful
            if(this.readyState === 4 && this.status === 200)
            {
            // Inserting the response from server into an HTML element
                let istoric = this.responseText.split('<div class="act-on-card__explanation">');
                istoric = istoric[1].split('</div>  </article>      </section>    </div>');
                var div = document.createElement("div");
                div.innerHTML =istoric[0];
                document.body.appendChild(div);
            }
        }
        request.send();
    }, 100);
    setInterval(function ()
    {
        if (window.location.href.indexOf("26444827") > -1) addGlobalStyle('body{background-color:darksalmon;}'); //difernt background for personal project 26444827
        else addGlobalStyle('body{background-color:black;}');
    }, 1000);
})();

function addGlobalStyle(css) {
    var head, style;
    head = document.getElementsByTagName('head')[0];
    if (!head) { return; }
    style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = css;
    head.appendChild(style);
}
