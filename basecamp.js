// ==UserScript==
// @name         Basecamp Add Tags
// @namespace    http://tampermonkey.net/
// @version      1.00007
// @description  Basecamp Add Tags on TODO list
// @author       Bogdan Simion
// @match        https://3.basecamp.com/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=basecamp.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    setInterval(function() {

        //check if page is dinamic reloaded
        check_div = document.getElementById('check_div');
        if (typeof(check_div) != 'undefined' && check_div != null)
        { } else {

        var drag_handle = document.getElementsByClassName('todo__drag-handle');
        if (drag_handle.length  === 0)
        {
            var drag_handle = document.getElementsByClassName('checkbox__label');
            var check_div = document.createElement("div");
            check_div.setAttribute("id", "check_div");
            document.body.appendChild(check_div);
        }

        //check if todo location only
        let ids = window.location.href.split('/');
        if (ids[4] != 'buckets') return false;

        addGlobalStyle('.click_me_:hover {text-shadow: -1px 0 gray, 0 1px gray, 1px 0 gray, 0 -1px gray;}');
        addGlobalStyle('.action-sheet.hide-on-setup {position:fixed; top:100px; right:10px}');
        addGlobalStyle('.todo__unassigned-unscheduled {position:absolute; margin-top:-18px}');



        //get all hoverable elements
        var userSelection = document.getElementsByClassName('todo__unassigned-unscheduled');
        for(let i = 0; i < userSelection.length; i++)
        {
            //Try to get extra element attached to this
            userSelection[i].innerHTML += '<a href='+userSelection[i].href+' class="click_me_ click_me_10_sidebar">[🍎10]</a>';
            userSelection[i].innerHTML += '<a href='+userSelection[i].href+' class="click_me_ click_me_9_sidebar">[🍌9]</a>';
            userSelection[i].innerHTML += '<a href='+userSelection[i].href+' class="click_me_ click_me_8_sidebar">[🍈8]</a>';
            userSelection[i].innerHTML += '<a href='+userSelection[i].href+' class="click_me_ click_me_7_sidebar">[🥚7]</a>';
            userSelection[i].innerHTML += '<a href='+userSelection[i].href+' class="click_me_ click_me_6_sidebar">🗑</a>';
            userSelection[i].href = 'javascript:void(0);';

        }

             pupulate_iframe("click_me_10_sidebar","[🍎10]");
             pupulate_iframe("click_me_9_sidebar","[🍌9]");
             pupulate_iframe("click_me_8_sidebar","[🍈8]");
             pupulate_iframe("click_me_7_sidebar","[🥚7]");
             pupulate_iframe("click_me_6_sidebar","");

             var check_div = document.createElement("div");
            check_div.setAttribute("id", "check_div");
            document.body.appendChild(check_div);
        }


 }, 100);

    function addGlobalStyle(css) {
    var head, style;
    head = document.getElementsByTagName('head')[0];
    if (!head) { return; }
    style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = css;
    head.appendChild(style);
}

    function pupulate_iframe(class_tag,html_prefix)
    {
        var userSelection2 = document.getElementsByClassName(class_tag);
        for(let i = 0; i < userSelection2.length; i++)
        {
            userSelection2[i].addEventListener("click", function()
            {
                event.preventDefault();
                addGlobalStyle('#pseudo_form_iframe { display:none}');

                //remove gray background from last selected element
                var old_elements=document.getElementsByClassName("highlight_style")
                for(let i = 0; i < old_elements.length; i++) {
                    old_elements[i].classList.remove("highlight_style")
                }

                //add gray background to curent selected element
                this.closest('span').classList.add("highlight_style");

                //get curent todo url and clean it
                let url =this.href.replace("?assign=true", "").replace("?schedule=true", "");

                //clear last iframe if exists
                if (document.getElementById("pseudo_form_iframe") !=null) document.getElementById("pseudo_form_iframe").outerHTML = "";

                var iframe = document.createElement("iframe");
                iframe.setAttribute("id", "pseudo_form_iframe");
                iframe.src = url;
                document.body.appendChild(iframe);
                iframe = document.getElementById('pseudo_form_iframe');

                iframe.addEventListener("load", function()
                {
                    let this_iframe = this.contentWindow.document;
                    this_iframe.getElementById('todo_content').innerHTML = html_prefix + this_iframe.getElementById('todo_content').innerHTML.replace(/\s*\[.*?\]\s*/g, '');
                    this_iframe.getElementById('todo_content').closest('form').submit();

                });
                addGlobalStyle('#pseudo_form_iframe { width: 50%;    float: right;    position: fixed;    top: -100px;    left: 50%; height:100% }');
                var todo_title_from_list = this.parentElement.parentElement.parentElement.parentElement.firstElementChild;
                todo_title_from_list.innerHTML = html_prefix + todo_title_from_list.innerHTML.replace(/\s*\[.*?\]\s*/g, '');
                setTimeout(function(){
                    var old_color = todo_title_from_list.style.color;
                    todo_title_from_list.style.color = "gray";
                    setTimeout(function(){
                        todo_title_from_list.style.color = old_color;
                    }, 1000)
                }, 300)

            })
        }
    }
})();
